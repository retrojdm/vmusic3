/*
VMusic3 Demo
Using the serial monitor

By Andrew Wyatt
retrojdm.com

Notes:  * Make sure you change the UART/SPI jumper on the VMusic3 over to SPI mode.
        * If you want the VMusic to reset when the Arduino does you'll need to switch its +5V power using a relay, controlled by PIN_VMUSIC_5V.


Open the serial monitor at 115200 baud and use the following commands:
(these are similar to WinAmp's keyboard shortcuts)

R         Toggle Repeat
S         Toggle Random

Z         Previous Track
X         Play/Restart/Unpause
C         Pause/Unpause
V         Stop
B         Next Track

< or ,    Previous Folder
> or .    Next Folder
*/

// VMusic SPI library
#include <vmusic3.h>

// VMusic SPI Pins
#define PIN_VMUSIC_MISO  2  // to VMusic MISO (brown)
#define PIN_VMUSIC_MOSI  4  // to VMusic MOSI (orange)
#define PIN_VMUSIC_SCK   5  // to VMusic CLK  (yellow)
#define PIN_VMUSIC_SS    6  // to VMusic SS#  (green)

#define PIN_VMUSIC_5V    3  // to VMusic 5V   (red) via an NPN Transistor or relay


// Create an instance of the VMusic3 class
VMusic3 vmusic(PIN_VMUSIC_MISO, PIN_VMUSIC_MOSI, PIN_VMUSIC_SCK, PIN_VMUSIC_SS);



void setup()
{
  // Power the VMusic up.
  pinMode(PIN_VMUSIC_5V, OUTPUT);
  digitalWrite(PIN_VMUSIC_5V, HIGH);

  // Start the Arduino's serial monitor
  delay(500);
  Serial.begin(115200);
  Serial.println("VMusic3 Demo");
}


void loop()
{    
  char strTrackTime[7];
    
  // Handle any events from the VMusic3
  switch (vmusic.loop()) {
  case VMUSIC_EVENT_NONE:
    break;

  case VMUSIC_EVENT_INITIALISED:
    Serial.println("VMusic3 Initialised!");
    break;

  case VMUSIC_EVENT_USB_DETECTED:
    Serial.println("USB Detected.");
    Serial.println("Searching...");
    break;

  case VMUSIC_EVENT_USB_REMOVED:
    Serial.println("USB Removed.");
    break;

  case VMUSIC_EVENT_TRACK_CHANGED:
    showTrackInfo();
    break;

  case VMUSIC_EVENT_TRACK_STOPPED:
    Serial.println("Track Stopped.");
    break;

  case VMUSIC_EVENT_TRACK_PROGRESSED:
    sprintf(strTrackTime, "%3u:%02u", vmusic.trackSeconds / 60, vmusic.trackSeconds % 60);
    Serial.println(strTrackTime);
    break;
  }
    
    
  // Read from the serial monitor to control the VMusic3.
  if (Serial.available() > 0)
  {
    switch(Serial.read())
    {  
    case 'r':
    case 'R':
      vmusic.keepPlaying = !(vmusic.keepPlaying);
      if (vmusic.keepPlaying) Serial.println("Keep Playing: on");
      else Serial.println("Keep Playing: off");
      break;
        
    case 's':
    case 'S':
      vmusic.randomMode = !(vmusic.randomMode);
      if (vmusic.randomMode) Serial.println("Random: on");
      else Serial.println("Random: off");
      break;

    case 'z':
    case 'Z':
      vmusic.prevTrack();
      showTrackInfo();
      break;

    case 'x':
    case 'X':
      // We set the volume here instead of in setup(), because if there's already a device attached,
      // we could be interfering with the events.
      vmusic.setVolume(VMUSIC_VOLUME_MAX); // 0..254
      vmusic.play();
      showTrackInfo();
      break;

    case 'c':
    case 'C':
      vmusic.pause();
      if (vmusic.mode == VMUSIC_MODE_PLAYING) Serial.println("Playing");
      else if (vmusic.mode == VMUSIC_MODE_PAUSED) Serial.println("Paused");
      break;

    case 'v':
    case 'V':
      vmusic.stop();
      Serial.println("Stopped");
      break;

    case 'b':
    case 'B':
      vmusic.nextTrack();
      showTrackInfo();
      break;

    case '<':
    case ',':
      vmusic.prevFolder();
      showTrackInfo();
      break;
    
    case '>':
    case '.':
      vmusic.nextFolder();
      showTrackInfo();
      break;
    }
  }
}


void showTrackInfo() {
    
  // display the track info
  if (strlen(vmusic.title) > 0)
  {
    Serial.print(vmusic.artist);
    Serial.print(" - ");
    Serial.print(vmusic.album);
    Serial.print(" - Track ");
    Serial.print(vmusic.trackNumber);
    Serial.print("/");
    Serial.print(vmusic.tracks);
    Serial.print(" - ");
    Serial.println(vmusic.title);
  }
  else
  {
    Serial.println(vmusic.filename);
  }
}