// VMusic3 SPI Demo
// Simple Example
// 
// By Andrew Wyatt
// retrojdm.com
// 
//
// Loads a file called "01-BAN~1.MP3", located in the folder "BANDOF~1\2009-B~1".
// You can find out the 8.3 format filenames for tracks on your USB stick by
// opening a DOS command prompt and using: DIR /X
//
//
// Notes:  * Make sure you change the UART/SPI jumper on the VMusic3 over to SPI mode.
//         * If you want the VMusic to reset when the Arduino does you'll need to switch
//           its +5V power using a relay, controlled by PIN_VMUSIC_5V.


// VMusic SPI library
#include <vmusic3.h>

// Remember, in C, the '\' character is an escape character.
// We need to use \\ for the backslashes.
#define PATH        "BANDOF~1\\2009-B~1"
#define FILENAME    "01-BAN~1.MP3"

// VMusic SPI Pins
#define PIN_VMUSIC_MISO  2  // to VMusic MISO (brown)
#define PIN_VMUSIC_MOSI  4  // to VMusic MOSI (orange)
#define PIN_VMUSIC_SCK   5  // to VMusic CLK  (yellow)
#define PIN_VMUSIC_SS    6  // to VMusic SS#  (green)

#define PIN_VMUSIC_5V    3  // to VMusic 5V   (red) via an NPN Transistor


// Create an instance of the VMusic3 class
VMusic3 vmusic(PIN_VMUSIC_MISO, PIN_VMUSIC_MOSI, PIN_VMUSIC_SCK, PIN_VMUSIC_SS);


void setup()
{
  pinMode(PIN_VMUSIC_5V, OUTPUT);
  digitalWrite(PIN_VMUSIC_5V, HIGH);

  delay(500);
  Serial.begin(115200);
  Serial.println("VMusic3 Simple Demo.");
}


void loop()
{    
  // Handle any events from the VMusic3
  switch (vmusic.loop())
  {    
  case VMUSIC_EVENT_NONE:
    break;

  case VMUSIC_EVENT_INITIALISED:
    Serial.println("VMusic3 Initialised!");
    tryToPlayTrack();
    break;        
    
  case VMUSIC_EVENT_USB_DETECTED:
    Serial.println("USB detected.");
    tryToPlayTrack();
    break;

  case VMUSIC_EVENT_USB_REMOVED:
    Serial.println("USB removed.");
    break;

  case VMUSIC_EVENT_TRACK_STOPPED:
    Serial.println("Track stopped.");
    break;

  case VMUSIC_EVENT_TRACK_CHANGED:
    Serial.println("Track changed.");
    break;

  case VMUSIC_EVENT_TRACK_PROGRESSED:
    Serial.print(".");
    break;
  }
}


void tryToPlayTrack()
{
  if (!vmusic.initialised || !vmusic.diskPresent) return;
      
  // We set the volume here instead of in setup(), because if there's already a device attached,
  // we could be interfering with the events.
  vmusic.setVolume(VMUSIC_VOLUME_MAX); // 0..254

  bool found = vmusic.loadTrack(PATH, FILENAME, 1, VMUSIC_MODE_PLAYING);
  if (found) {
    Serial.println("Playing Track.");
  }
  else
  {
    Serial.print("Track \"");
    Serial.print(PATH);
    Serial.print("\\");
    Serial.print(FILENAME);
    Serial.println("\" not found.");
  }     
}