#include "vmusic3.h"

////////////////////////////////////////////////////////////////////////////////
// Class Constructor
VMusic3::VMusic3(
    int miso,    // brown wire
    int mosi,    // orange wire
    int sck,     // yellow wire
    int ss)      // green wire
{

    // This library uses SPI to communicate with the VMusic3.    
    // Remember the pins.
    // These are used in the _transfer() function.
    _miso           = miso;
    _mosi           = mosi;
    _sck            = sck;
    _ss             = ss;

    // Set their modes
    pinMode(_miso,  INPUT);
    pinMode(_mosi,  OUTPUT);
    pinMode(_sck,   OUTPUT);
    pinMode(_ss,    OUTPUT);
    
    
    // Initialise the attributes.
    initialised         = false;
    volume				= 0;
    keepPlaying			= false;
    repeatFolder		= false;
    randomMode			= false;
    tracks				= 0;
    trackNumber			= 0;
    
	mode				= VMUSIC_MODE_STOPPED;
	diskPresent			= false;
	fileIsOpen			= false;
	path[0]				= VMUSIC_CHAR_NULL;
	filename[0]			= VMUSIC_CHAR_NULL;

	_clearTrackInfo();

	_lastCommand		= 0x00; // None
	_lastResponse[0]	= VMUSIC_CHAR_NULL;
	_justSeeked			= false;
	_lookingForTrack	= false;

    #ifdef VMUSIC_USE_DIR_TABLE
        // The lookup table has not yet been populated.
		_dirTableLength = 0;
		_dirTableHasBeenRead = false;
    #endif    
}


// Call this function in your sketch's loop().
//
// Reads from the VMusic3.
// If the data is new, it handles the response.
//
// returns one of the event codes:
//
//  VMUSIC_EVENT_NONE
//  VMUSIC_EVENT_INITIALISED
//  VMUSIC_EVENT_USB_DETECTED
//  VMUSIC_EVENT_USB_REMOVED
//  VMUSIC_EVENT_TRACK_STOPPED
//  VMUSIC_EVENT_TRACK_CHANGED
//  VMUSIC_EVENT_TRACK_PROGRESSED
//
VMusicEvent VMusic3::loop()
{
	if (_getResponse() == VMUSIC_SPI_DATA_IS_NEW)
	{
		VMusicEvent response = _handleResponse();
		if (response != VMUSIC_EVENT_NONE) return response;
	}


    // if the DD2 event was detected, "_lookingForTrack" would have been set on the previous pass of loop().
    if (initialised && _lookingForTrack)
    {
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.print("VMusic3::Looking for the first track... ");
        #endif

        #ifdef VMUSIC_USE_DIR_TABLE
            // The lookup table has not yet been populated.
            _dirTableLength = 0;
            _dirTableHasBeenRead = false;
        #endif            

		_lookingForTrack = false;
        
        // Look in the root directory for tracks.
        if (strcmp(_lastResponse, VMUSIC_RESP_PROMPT) != 0)
            _waitForPrompt();
        _findItemInCurrentDir(filename, VMUSIC_EMPTY_STRING, VMUSIC_ITEMTYPE_TRACK, VMUSIC_FIND_FIRST);

        // Success?
		bool found = false;
        if (strlen(filename) > 0)
        {
            found = true;
            trackNumber = 1;
            #ifdef VMUSIC_DEBUG_VERBOSE
                if (found) Serial.println("VMusic3::Found in root.");
            #endif
        }
        else
        {
            // If no track was found, Try to find a folder with tracks in it.
            found = _findFolderWithTracks(VMUSIC_DIRECTION_NEXT, VMUSIC_FIND_FIRST);
			#ifdef VMUSIC_DEBUG_VERBOSE
				if (found) Serial.println("VMusic3::Found in dir.");
			#endif
        }

        #ifdef VMUSIC_DEBUG_VERBOSE
            if (!found) Serial.println("VMusic3::Not Found.");
        #endif

        if (found) return VMUSIC_EVENT_TRACK_CHANGED;
        else return VMUSIC_EVENT_TRACK_STOPPED;
    }

    return VMUSIC_EVENT_NONE;
}


// Sets the volume.
// "bytVolume" can be a value between 0 and 254.
void VMusic3::setVolume(byte newVolume)
{
    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.print("VMusic3::setVolume(");
        Serial.print(newVolume);
        Serial.println(")");
    #endif

    if (!initialised)
    {
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.println("VMusic3::Not initialised yet.");
        #endif
        return;
    }

    newVolume   = constrain(newVolume, VMUSIC_VOLUME_MIN, VMUSIC_VOLUME_MAX);
    volume      = newVolume; // Remember it

    // Invert the volume so that 0 is minimum and 254 is maximum.
    // It's confusing the other way around.
    newVolume   = VMUSIC_VOLUME_MAX - newVolume;

    // UM_VinculumFirmware_V205.pdf - page 53.
	_lastCommand = VMUSIC_CMD_VSV;
	_write(VMUSIC_CMD_VSV);         // Sets playback volume
	_write(VMUSIC_CHAR_SPACE);
	_write(newVolume);       // Byte 0x00 max .. 0xFE min
	_write(VMUSIC_CHAR_RETURN);
	_waitForPrompt();
}


// Tries to load a particular track.
// Specify the path and filename of the track, and the trackNumber you want to display.
// intMode can be either STOPPED or PLAYING
// Eg:
// newPath:        "80S"
// newFilename:    "AIRSUP~1.MP3"
// newTrackNumber: 4
// newMode:        stopped
//
// Why do you need to provide the track number?
// --------------------------------------------
// When looking up tracks, The DIR command doesn't return files in alphabetical order - so
// it's hard to tell which track number the file is.
// There's not enough SRAM to hold an array of tracks for sorting (and determine it's position).   
// Instead, you need to provide it as a parameter.
// Hopefully if you're loading a track, you've remembered it's track number too.
bool VMusic3::loadTrack(const char *newPath, const char *newFilename, int newTrackNumber, VMusicMode newMode)
{
    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.print("VMusic3::loadTrack(\"");
        Serial.print(newPath);
        Serial.print("\", \"");
        Serial.print(newFilename);
        Serial.print("\", ");
        Serial.print(newTrackNumber);
        Serial.print(", ");
        Serial.print(newMode);
        Serial.println(")");
    #endif

    if (!initialised)
    {
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.println("VMusic3::Not initialised yet.");
        #endif
        return false;
    }

    int     position;
    char    folder[13];

	_lookingForTrack = false;

    if (diskPresent)
    {
        // We can't perform disk operations while a file is open.
        if (fileIsOpen) stop();

		_clearTrackInfo();
        trackNumber = 0;

        // If we're not already in the specified folder...
        if (strcmp(path, newPath) != 0)
        {
            // if we're not in the root directory, we need to go all the way back.
            while (strlen(path) > 0) _changeDir(VMUSIC_PARENT_DIR);

            // Starting at the beginning, we extract each folder name from the path, and try to go into it.
            // Notice the "<=" below? We WANT to see the null terminator, so it can be treated the same as a backslash.
            position = 0;
            for (size_t c = 0; c <= strlen(newPath); c++)
            {
                // Did we find a backslash or null terminator?
                if ((newPath[c] == '\\') || (newPath[c] == VMUSIC_CHAR_NULL))
                {
                    strlcpy(folder, &(newPath[position]), c - position + 1);
                    if (!_changeDir(folder)) break;
                    position = c + 1;
                }
            }
        }

        // Did we make it to the specified path?
        if (strcmp(path, newPath) == 0)
        {
            
            // Try to find the file.
            if (_trackExists(newFilename))
            {
                // Since we never made a call to _findFolderWithTracks(), we need to generate the list here.
				_generateShuffledTrackList();

                // We also won't have a lookup table for this directory yet.
                #ifdef VMUSIC_USE_DIR_TABLE
				    _populateDirTable();
                #endif

                strlcpy(filename, newFilename, sizeof(filename));
                _getTrackInfo(newMode);
                trackNumber = newTrackNumber;
                return true;
            }
            else nextTrack();
        }
    }

    return false;
}



void VMusic3::play()
{
    // Tries to play the current track.
    // Populates the .title, .artist, and .album attributes with the ID3 info if it's available.

    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.println("VMusic3::play()");
    #endif

    if (!initialised)
    {
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.println("VMusic3::Not initialised yet.");
        #endif
        return;
    }

    if (fileIsOpen) stop();

    if (diskPresent && (strlen(filename) > 0))
    {
        // UM_VinculumFirmware_V205.pdf - page 52.

		_lastCommand = VMUSIC_CMD_VPF;
		_write(VMUSIC_CMD_VPF); // Plays a single file 
		_write(VMUSIC_CHAR_SPACE);
        for (byte c = 0; c < strlen(filename) ; c++) _write(filename[c]);
		_write(VMUSIC_CHAR_RETURN);
        
		_waitForResponse();
        
        if (
            (_lastResponse[0] == 'P') &&
            (_lastResponse[1] == 'l') &&
            (_lastResponse[2] == 'a') &&
            (_lastResponse[3] == 'y') &&
            (_lastResponse[4] == 'i') &&
            (_lastResponse[5] == 'n') &&
            (_lastResponse[6] == 'g') &&
            (_lastResponse[7] == VMUSIC_CHAR_SPACE))
        {
            fileIsOpen = true;

            // What's the next response?
            // We're expecting either the ID3 Track field, or the 'T' response if there's no ID3 info.
			_waitForResponse();

            // If the next response IS NOT 'T' then we assume there's ID3 info coming.
            if (_lastResponse[0] != 'T')
            {
                // Get the ID3 info

                // <Track>
                //strlcpy(track, _lastResponse, sizeof(track)); // To save SRAM, we don't store this

                // <Title>
				_waitForResponse();
                strlcpy(title, _lastResponse, sizeof(title));

                // <Artist>
				_waitForResponse();
                strlcpy(artist, _lastResponse, sizeof(artist));

                // <Album>
				_waitForResponse();
                strlcpy(album, _lastResponse, sizeof(album));

                // <Composer>
				_waitForResponse();
                //strlcpy(composer, _lastResponse, sizeof(composer)); // To save SRAM, we don't store this
            }

            trackSeconds    = 0;
            mode            = VMUSIC_MODE_PLAYING;

        }
        else
        {
            trackNumber     = 0;
			_clearTrackInfo();
            mode            = VMUSIC_MODE_STOPPED;
            fileIsOpen      = false;
        }
    }
}



void VMusic3::pause()
{
    // If playing or paused, this toggles between playing/paused.

    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.println("VMusic3::pause()");
    #endif

    if (!initialised)
    {
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.println("VMusic3::Not initialised yet.");
        #endif
        return;
    }

    if (fileIsOpen)
    {
        if ((mode == VMUSIC_MODE_PLAYING) || (mode == VMUSIC_MODE_PAUSED))
        {
            // UM_VinculumFirmware_V205.pdf - page 53.
			_lastCommand = VMUSIC_CMD_VP;
			_write(VMUSIC_CMD_VP);  // Pause playback (unpause if paused)
			_write(VMUSIC_CHAR_RETURN);
			_waitForPrompt();

            // Set the mode
            mode = (mode == VMUSIC_MODE_PLAYING) ? VMUSIC_MODE_PAUSED : VMUSIC_MODE_PLAYING;
        }
    }
}



void VMusic3::stop()
{
    // Stops a track.

    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.println("VMusic3::stop()");
    #endif

    if (!initialised)
    {
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.println("VMusic3::Not initialised yet.");
        #endif
        return;
    }

    if (fileIsOpen)
    {
		_lastCommand = VMUSIC_CMD_VST;
		_write(VMUSIC_CMD_VST); // Stops playback
		_write(VMUSIC_CHAR_RETURN);
    
        do { _waitForResponse(); } while (strcmp(_lastResponse, "Stopped") != 0);
		_waitForPrompt();

        mode            = VMUSIC_MODE_STOPPED;
        trackSeconds    = 0;
        fileIsOpen      = false;
    }
}



void VMusic3::fastForward()
{
    // Fast forward 5 seconds.

    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.println("VMusic3::fastForward()");
    #endif

    if (!initialised)
    {
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.println("VMusic3::Not initialised yet.");
        #endif
        return;
    }

    if (fileIsOpen)
    {
		_lastCommand = VMUSIC_CMD_VF;
		_write(VMUSIC_CMD_VF);
		_write(VMUSIC_CHAR_RETURN);
		_justSeeked = true;
    }
}



void VMusic3::rewind()
{
    // Rewind 5 seconds.

    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.println("VMusic3::rewind()");
    #endif

    if (!initialised)
    {
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.println("VMusic3::Not initialised yet.");
        #endif
        return;
    }

    if (fileIsOpen)
    {
		_lastCommand = VMUSIC_CMD_VB;
		_write(VMUSIC_CMD_VB);
		_write(VMUSIC_CHAR_RETURN);
		_justSeeked = true;
    }
}


// Goes to the previous track, or a random one within the folder.
// If PLAYING, the new track will start playing.
// Returns true if successful.
bool VMusic3::prevTrack()
{
    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.println("VMusic3::prevTrack()");
    #endif

    if (!initialised)
    {
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.println("VMusic3::Not initialised yet.");
        #endif
        return false;
    }

    if (randomMode) return _randomTrack(VMUSIC_DIRECTION_PREVIOUS);
    else return _findTrack(VMUSIC_DIRECTION_PREVIOUS);
}


// Goes to the next track, or a random one within the folder.
// If PLAYING, the new track will start playing.
// Returns true if successful.
bool VMusic3::nextTrack()
{
    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.println("VMusic3::nextTrack()");
    #endif

    if (!initialised)
    {
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.println("VMusic3::Not initialised yet.");
        #endif
        return false;
    }

    if (randomMode) return _randomTrack(VMUSIC_DIRECTION_NEXT);
    else return _findTrack(VMUSIC_DIRECTION_NEXT);
}


// Tries to go to the previous folder that has tracks in it.
// Returns true if successful.
bool VMusic3::prevFolder()
{
    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.println("VMusic3::prevFolder()");
    #endif

    if (!initialised)
    {
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.println("VMusic3::Not initialised yet.");
        #endif
        return false;
    }

    return _findFolderWithTracks(VMUSIC_DIRECTION_PREVIOUS, VMUSIC_FIND_FIRST);
}


// Tries to go to the next folder that has tracks in it.
// Returns true if successful.
bool VMusic3::nextFolder()
{
    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.println("VMusic3::nextFolder()");
    #endif

    if (!initialised)
    {
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.println("VMusic3::Not initialised yet.");
        #endif
        return false;
    }

    return _findFolderWithTracks(VMUSIC_DIRECTION_NEXT, VMUSIC_FIND_FIRST);
}



////////////////////////////////////////////////////////////////////////////////
// Private Functions

// Called when a new response comes in.
// Deals with the response, depending on the current state.
// Returns an event code.
VMusicEvent VMusic3::_handleResponse()
{
	// Received ">" Prompt.
	if (strcmp(_lastResponse, VMUSIC_RESP_PROMPT) == 0)
		return _handleResponsePrompt();

	// Track seconds
	if ((mode == VMUSIC_MODE_PLAYING) && (_lastResponse[0] == 'T') && (_lastResponse[1] == VMUSIC_CHAR_SPACE))
		return _handleResponseTrackSeconds();

	// Track finished (or device removed while playing).
	// If the device is removed, the "Stopped" event comes in before the "DR2" event.
	if (strcmp(_lastResponse, "Stopped") == 0)
    {
		return _handleResponseStopped();
    }

	// Device Detected
	if ((strcmp(_lastResponse, VMUSIC_EVENT_DD2) == 0) || (strcmp(_lastResponse, VMUSIC_EVENT_DD2_EXTENDED) == 0))
		return _handleResponseDeviceDetected();

    // No Upgrade
    if ((strcmp(_lastResponse, VMUSIC_RESP_NU) == 0) || (strcmp(_lastResponse, VMUSIC_RESP_NU_EXTENDED) == 0))
        return _handleResponseNoUpgrade();

	// Device Removed
	if (strcmp(_lastResponse, VMUSIC_EVENT_DR2) == 0)
		return _handleResponseDeviceRemoved();

	// Received "".
	if (strlen(_lastResponse) == 0)
		return _handleResponseBlank();

	// Received "Ver ...".
	if ((_lastResponse[0] == 'V') &&
		(_lastResponse[1] == 'e') &&
		(_lastResponse[2] == 'r') &&
		(_lastResponse[3] == ' '))
		return _handleResponseVersion();

    // No Disk
    if ((strcmp(_lastResponse, VMUSIC_RESP_ND) == 0) || (strcmp(_lastResponse, VMUSIC_RESP_ND_EXTENDED) == 0))
        return _handleResponseNoDisk();

	// Received "D:/>" Prompt.
	// This is last, as an optimisation so we don't keep checking for it before other more likely responses.
	if (strcmp(_lastResponse, VMUSIC_RESP_PROMPT_EXTENDED) == 0)
		return _handleResponsePromptExtended();

    // Command Failed
    if ((strcmp(_lastResponse, VMUSIC_RESP_CF) == 0) || (strcmp(_lastResponse, VMUSIC_RESP_CF_EXTENDED) == 0))
        return _handleResponseCommandFailed();

	#ifdef VMUSIC_DEBUG_VERBOSE
		Serial.print("VMusic3::Unhandled response: \"");
		Serial.print(_lastResponse);
		Serial.println("\".");
	#endif
	return VMUSIC_EVENT_NONE;
}


// Handle ">".
VMusicEvent VMusic3::_handleResponsePrompt()
{
	switch (_lastCommand)
	{
	case VMUSIC_CMD_SCS: // Successfully entered "Short Command Set" mode
		#ifdef VMUSIC_DEBUG_VERBOSE
			Serial.println("VMusic3::Initialised.");
		#endif
		initialised = true;
        return VMUSIC_EVENT_INITIALISED;
        break;

	case VMUSIC_CMD_CD:
		break;

	case VMUSIC_CMD_DIR:
		break;

	case VMUSIC_CMD_VSV:
		break;

	case VMUSIC_CMD_VPF:
		break;

	case VMUSIC_CMD_VP:
		break;

	case VMUSIC_CMD_VST:
		break;

	case VMUSIC_CMD_VF:
		break;

	case VMUSIC_CMD_VB:
		break;
	}

	return VMUSIC_EVENT_NONE;
}


// Handle "T xx".
VMusicEvent VMusic3::_handleResponseTrackSeconds()
{
	// This is a hack to try and fix the bug where a prompt is sometimes muddled with the "T xx" response
	// after a fastForward() or rewind() command while playing.
	int trackTimeDataOffset = 0;
	if ((_justSeeked) && (_lastResponse[2] == VMUSIC_CHAR_PROMPT))
	{
		trackTimeDataOffset = 2;
		_justSeeked = false;
	}

	// Note: numerical data is received LSB first.
	trackSeconds = ((unsigned char)(_lastResponse[3 + trackTimeDataOffset]) * 256) + (unsigned char)(_lastResponse[2 + trackTimeDataOffset]);

	#ifdef VMUSIC_DEBUG_VERBOSE
		char buffer[21];
		Serial.print("VMusic3::Track Progressed (");
		sprintf(buffer, "0x%02x 0x%02x)+%d: %u", (unsigned char)(_lastResponse[2 + trackTimeDataOffset]), (unsigned char)(_lastResponse[3 + trackTimeDataOffset]), trackTimeDataOffset, trackSeconds);
		Serial.println(buffer);
	#endif

	return VMUSIC_EVENT_TRACK_PROGRESSED;
}


// Handle "Stopped".
VMusicEvent VMusic3::_handleResponseStopped()
{
	fileIsOpen = false;
	trackSeconds = 0;

	//_waitForPrompt(); // <- This also determines if there's still a disk present

	if (!diskPresent)
	{
		#ifdef VMUSIC_DEBUG_VERBOSE
			Serial.println("VMusic3::USB Removed while playing.");
		#endif

		#ifdef VMUSIC_USE_DIR_TABLE
			// The lookup table has not yet been populated.
			_dirTableLength = 0;
			_dirTableHasBeenRead = false;
		#endif

		return VMUSIC_EVENT_USB_REMOVED;
	}

	else if (mode == VMUSIC_MODE_PLAYING)
	{
		if (keepPlaying)
		{
			#ifdef VMUSIC_DEBUG_VERBOSE
				Serial.println("VMusic3::Keep Playing.");
			#endif

			// Next (or Random)
            _waitForPrompt();
			if (nextTrack())
			{
				play();
				#ifdef VMUSIC_DEBUG_VERBOSE
					Serial.println("VMusic3::Next Track.");
				#endif
				return VMUSIC_EVENT_TRACK_CHANGED;
			}
		}
	}

	mode = VMUSIC_MODE_STOPPED;
	#ifdef VMUSIC_DEBUG_VERBOSE
		Serial.println("VMusic3::Track Stopped.");
	#endif

	return VMUSIC_EVENT_TRACK_STOPPED;
}


// Handle "DD2" (and "Device Detected P2", since it can happen before we enter SCS mode upon startup).
VMusicEvent VMusic3::_handleResponseDeviceDetected()
{

	diskPresent = true;
	fileIsOpen	= false;
	mode		= VMUSIC_MODE_STOPPED;

	_clearTrackInfo();

	#ifdef VMUSIC_DEBUG_VERBOSE
		Serial.println("VMusic3::USB Detected.");
	#endif

	return VMUSIC_EVENT_NONE;
}


// Handle "NU".
VMusicEvent VMusic3::_handleResponseNoUpgrade()
{
    // We'll look for tracks on the next pass of the loop.
    // This allows your sketch to display a string like "Searching..." while we walk the directory tree looking for a track to play.
    _lookingForTrack = true;

    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.println("VMusic3::No Upgrade.");
    #endif

    return VMUSIC_EVENT_USB_DETECTED;
}


// Handle "DR2".
VMusicEvent VMusic3::_handleResponseDeviceRemoved()
{
	_markAsStopped();

	#ifdef VMUSIC_DEBUG_VERBOSE
		Serial.println("VMusic3::USB Removed.");
	#endif

	#ifdef VMUSIC_USE_DIR_TABLE
		// The lookup table has not yet been populated.
		_dirTableLength = 0;
		_dirTableHasBeenRead = false;
	#endif

	return VMUSIC_EVENT_USB_REMOVED;
}


// Handle "".
VMusicEvent VMusic3::_handleResponseBlank()
{
    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.println("VMusic3::Blank response.");
    #endif

	return VMUSIC_EVENT_NONE;
}


// Handle "Ver ...".
VMusicEvent VMusic3::_handleResponseVersion()
{
    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.print("VMusic3:: ");
        Serial.println(_lastResponse);
    #endif

	return VMUSIC_EVENT_NONE;
}


// Handle "No Disk" or "ND".
VMusicEvent VMusic3::_handleResponseNoDisk()
{
    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.println("VMusic3:: No disk.");
    #endif

    _markAsStopped();
    if (!initialised) _tryToEnterScsMode();
    
    return VMUSIC_EVENT_NONE;
}


// Handle "D:\>".
VMusicEvent VMusic3::_handleResponsePromptExtended()
{
	_tryToEnterScsMode();

	return VMUSIC_EVENT_NONE;
}


VMusicEvent VMusic3::_handleResponseCommandFailed()
{
    #ifdef VMUSIC_DEBUG_VERBOSE
        char buffer[8];
        sprintf(buffer, "0x%02x", _lastCommand);
        Serial.print("VMusic3:: Command failed: ");
        Serial.println(buffer);
    #endif
    
    _write(VMUSIC_CHAR_RETURN);
    _waitForResponse();

    return VMUSIC_EVENT_NONE;
}


void VMusic3::_tryToEnterScsMode()
{
    #ifdef VMUSIC_DEBUG_VERBOSE
       Serial.println("VMusic3::Trying to enter SCS Mode...");
    #endif

    _lastCommand = VMUSIC_CMD_SCS;
    _write(VMUSIC_CMD_SCS);
    _write(VMUSIC_CHAR_RETURN);
}


void VMusic3::_markAsStopped()
{
    diskPresent     = false;
    fileIsOpen      = false;
    mode            = VMUSIC_MODE_STOPPED;

    path        [0] = '\0';
    filename    [0] = '\0';
    trackNumber     = 0;

    _clearTrackInfo();
}


// Takes a response from the DIR command, and returns true if the item is a folder.
bool VMusic3::_isFolder(const char *responseToCheck)
{
    // Returns true if the last 4 characters are " DIR".
    size_t len = strlen(responseToCheck);
    if (len > 4)
    {
        if ((strcmp(responseToCheck, ". DIR") == 0) || (strcmp(responseToCheck, ".. DIR") == 0)) return false;
        return ((responseToCheck[len - 4] == VMUSIC_CHAR_SPACE) && (responseToCheck[len - 3] == 'D') && (responseToCheck[len - 2] == 'I') && (responseToCheck[len - 1] == 'R'));
    }
    return false;
}


// Takes a response from the DIR command, and returns true if the item is an .MP3 or .WMA file.
bool VMusic3::_isTrack(const char *filenameToCheck)
{
	size_t len = strlen(filenameToCheck);
    if (len > 4)
    {
        return (
            (filenameToCheck[len - 4] == '.') && 
            (
                ((filenameToCheck[len - 3] == 'M') && (filenameToCheck[len - 2] == 'P') && (filenameToCheck[len - 1] == '3')) ||
                ((filenameToCheck[len - 3] == 'W') && (filenameToCheck[len - 2] == 'M') && (filenameToCheck[len - 1] == 'A'))
            )
        );
    }
    return false;
}


void VMusic3::_removeDirSuffix(char *folderResponse)
{    
    #ifdef VMUSIC_DEBUG_VERBOSE
       Serial.print("VMusic3::_removeDirSuffix(\"");
       Serial.print(folderResponse);
       Serial.print("\"): \"");
    #endif

    folderResponse[strlen(folderResponse) - 4] = VMUSIC_CHAR_NULL; // Chop off the " DIR" at the end.

    #ifdef VMUSIC_DEBUG_VERBOSE
       Serial.print(folderResponse);
       Serial.println("\"");
    #endif
}


// Returns true if the file exists within the current folder.
// Also sets the .tracks attribute (number of tracks in the current folder).
bool VMusic3::_trackExists(const char *filenameToFind)
{
    bool found = false;
    tracks      = 0;
    
    if (diskPresent)
    {
        #ifdef VMUSIC_USE_DIR_TABLE

            // Have we looked in the dir yet?
            if (!_dirTableHasBeenRead) _populateDirTable();

            // Does it exist in the lookup table?
            for (int i = 0; i < _dirTableLength; i++)
            {
                if (
                    (_dirTable[i].isFolder) &&
                    (strcmp(filenameToFind, _dirTable[i].filename) == 0)
                ) return true;
            }
            return false;

        #else
    
            // DIR
			_lastCommand = VMUSIC_CMD_DIR;
			_write(VMUSIC_CMD_DIR);
			_write(VMUSIC_CHAR_RETURN);
			_waitForResponse();

            // Go through each file/folder in the current directory.
            while(1)
            {
                // Done?
				_waitForResponse();
                if ((_lastResponse[0] == VMUSIC_CHAR_PROMPT) && (_lastResponse[1] == VMUSIC_CHAR_NULL)) break;

                // Is the current item an .MP3 or .WMA file?
                if (_isTrack(_lastResponse))
                {
                    tracks++;
                    
                    // Found?
                    if (strcmp(filenameToFind, _lastResponse) == 0) found = true;

                    // We can't break here, because we need to read the rest of the responses to clear them.
                }
            }

        #endif
    }

    return found;
}


// Tries to change directory.
// Use PARENT_DIR to go back one level.
// Returns true if successful.
bool VMusic3::_changeDir(const char *newDir)
{    
    #ifdef VMUSIC_USE_DIR_TABLE
		_dirTableHasBeenRead = false;
    #endif

    if (diskPresent)
    {
        // Send the CD command
		_lastCommand = VMUSIC_CMD_CD;
		_write(VMUSIC_CMD_CD);
		_write(VMUSIC_CHAR_SPACE);
        for (byte c = 0; c < strlen(newDir); c++) _write(newDir[c]);
		_write(VMUSIC_CHAR_RETURN);

        // Were we successful?
		_waitForResponse();
        if (strcmp(_lastResponse, VMUSIC_RESP_PROMPT) == 0)
        {
            // We need to maintain a "path" string, since there's no way to ask the VMusic.
            
            // We don't want to append the "." directory to the path.
            if (strcmp(newDir, ".") != 0)
            {
                if (strcmp(newDir, VMUSIC_PARENT_DIR) == 0)
                {
                    // We went up one level. We need to chop that last dir off the path.
                    for (int c = strlen(path) - 1; c >= 0; c--)
                    {
                        char ch = path[c];
                        path[c] = VMUSIC_CHAR_NULL;
                        if (ch == '\\') break;
                    }
                }
                else
                {
                    // Append the current directory to the path.
                    if (strlen(path) > 0) strcat(path, "\\");
                    strcat(path, newDir);
                }

                tracks = 0; // We'll need to call _findItemInCurrentDir() or _trackExists() again at some point to find out how many tracks are in this new folder.
            }

            return true;
        }
    }
	return false;
}



// Gets the endmost folder in a path.
// Copies the result to the given "strResult" buffer.
// Eg: _getEndMostFolderFromPath("MUSIC\ARTIST\ALBUM") should return "ALBUM".
void VMusic3::_getEndMostFolderFromPath(char *result)
{
    int pos = 0;
    for (int c = strlen(path) - 1; c >= 0; c--)
    {
        if (path[c] == '\\')
        {
            pos = c + 1;
            break;
        }
    }
    strlcpy(result, &(path[pos]), 13);
}



// Tries to find a file in the current directory.
//
// "result" will be set to the filename of the item found.
//
// "currentItem" specifies an item we want to find either the previous or next item relative to.
// (We can use VMUSIC_EMPTY_STRING if looking for the first or last item)
//
// "findFolder" specifies if we're looking for a track or folder. Eg: FIND_TRACK, or FIND_FOLDER.
//
// "position" determines which item we're looking for. Eg: FIND_FIRST, FIND_PREV, FIND_NEXT, or FIND_LAST.
void VMusic3::_findItemInCurrentDir(char *result, const char *currentItem, VMusicItemType findItemType, VMusicFind position)
{
    bool isFolder     = false;
    bool isTrack      = false;
    bool foundNewBest = false;

    // Start with nothing.
    result[0]   = VMUSIC_CHAR_NULL;
    tracks      = 0;

    if (diskPresent)
    {
        #ifdef VMUSIC_USE_DIR_TABLE

            if (!_dirTableHasBeenRead) _populateDirTable();
            int dirTableIndex = 0;

        #else

            // List files and folders in the the directory
			_lastCommand = VMUSIC_CMD_DIR;
			_write(VMUSIC_CMD_DIR);
			_write(VMUSIC_CHAR_RETURN);
			_waitForResponse();

        #endif

        // Check each item returned by the DIR command.
        while(1)
        {
            #ifdef VMUSIC_USE_DIR_TABLE

                // Got to the end of the _dirTable (or it was empty)?
                if (dirTableIndex >= _dirTableLength) break;

                // copy the item from the _dirTable to the _lastResponse.
                strlcpy(
					_lastResponse,
					_dirTable[dirTableIndex].filename,
                    13
                    );
                isFolder = _dirTable[dirTableIndex].isFolder;
                isTrack  = !isFolder;

            #else

                // Prompt = Done.
				_waitForResponse();
                if ((_lastResponse[0] == VMUSIC_CHAR_PROMPT) && (_lastResponse[1] == VMUSIC_CHAR_NULL)) break;

                // What type of item is it?
                isFolder   = _isFolder(_lastResponse);
                isTrack    = _isTrack(_lastResponse);
                if (isFolder) _removeDirSuffix(_lastResponse);

            #endif

            if (isTrack) tracks++;

            // If we're in folder mode and it's a folder, or we're in track mode and it's a track...
            if (((findItemType == VMUSIC_ITEMTYPE_FOLDER) && isFolder) || ((findItemType == VMUSIC_ITEMTYPE_TRACK) && isTrack))
            {
                foundNewBest = false;

                // What are we looking for?
                switch (position)
                {

                case VMUSIC_FIND_FIRST:
                    if (
                        (strlen(result) == 0) ||
                        (strcmp(_lastResponse, result) < 0)
                    ) foundNewBest = true;
                    break;

                case VMUSIC_FIND_PREVIOUS:
                    // If it's before the "current" dir...
                    if (strcmp(_lastResponse, currentItem) < 0)
                    {
                        // Set it as the prev one if we haven't found the prev one yet,
                        // or this one is after the current prev one.
                        if (
                            (strlen(result) == 0) ||
                            (strcmp(_lastResponse, result) > 0)
                        ) foundNewBest = true;
                    }
                    break;

                case VMUSIC_FIND_NEXT:
                    // If it's after the "current" dir...
                    if (strcmp(_lastResponse, currentItem) > 0)
                    {
                        // Set it as the next one if we haven't found the next one yet,
                        // or this one is before the current next one.
                        if (
                            (strlen(result) == 0) ||
                            (strcmp(_lastResponse, result) < 0)
                        ) foundNewBest = true;
                    }
                    break;

                case VMUSIC_FIND_LAST:
                    if (
                        (strlen(result) == 0) ||
                        (strcmp(_lastResponse, result) > 0)
                    ) foundNewBest = true;
                    break;
                }
                
                // If a better candidate was found, set it.
                if (foundNewBest) strlcpy(result, _lastResponse, 13);
            }

            
            #ifdef VMUSIC_USE_DIR_TABLE

                // Check out the next item in the dir table.
				dirTableIndex++;

            #endif            
        }
    }
}


// Tries to find either the previous or next track.
// intDir can be either previous or next.
//
// Returns true if successful.
bool VMusic3::_findTrack(VMusicDirection direction)
{
    bool		fileFound	= false;
    VMusicMode	oldMode		= mode;
    char		result[13];
        
    result[0] = VMUSIC_CHAR_NULL;
    
    // Reset the track info (but NOT the track number).
    // We're assuming the "tracks" attribute has already been set by a previous call to _findItemInCurrentDir().
	_clearTrackInfo();

    if (diskPresent)
    {
        // You must stop a file if it's playing before you can play another one.
        if (fileIsOpen) stop();

        // Are we trying to go to the previous track?
        if (direction == VMUSIC_DIRECTION_PREVIOUS)
        {
            // Can we go back one?
            if (trackNumber > 1)
            {
				_findItemInCurrentDir(result, filename, VMUSIC_ITEMTYPE_TRACK, VMUSIC_FIND_PREVIOUS);
                strlcpy(filename, result, sizeof(filename));
                trackNumber--;
            }
            else
            {
                // No? Then do we loop within the current folder, or go to the previous folder?
                if (repeatFolder)
                {
					_findItemInCurrentDir(result, filename, VMUSIC_ITEMTYPE_TRACK, VMUSIC_FIND_LAST);
                    strlcpy(filename, result, sizeof(filename));
                    trackNumber = tracks;
                }
                else _findFolderWithTracks(VMUSIC_DIRECTION_PREVIOUS, VMUSIC_FIND_LAST);
            }

        }
        else if (direction == VMUSIC_DIRECTION_NEXT)
        {

            // Can we go forward one?
            if (trackNumber < tracks)
            {
				_findItemInCurrentDir(result, filename, VMUSIC_ITEMTYPE_TRACK, VMUSIC_FIND_NEXT);
                strlcpy(filename, result, sizeof(filename));
                trackNumber++;
            }
            else
            {
                // No? Then do we loop within the current folder, or go to the next folder?
                if (repeatFolder)
                {
					_findItemInCurrentDir(result, filename, VMUSIC_ITEMTYPE_TRACK, VMUSIC_FIND_FIRST);
                    strlcpy(filename, result, sizeof(filename));
                    trackNumber = 1;
                }
                else _findFolderWithTracks(VMUSIC_DIRECTION_NEXT, VMUSIC_FIND_FIRST);
            }
        }

        // Did we find a new track?
        fileFound = (strlen(filename) > 0);
        if (fileFound) _getTrackInfo(oldMode); // Get it's info.
    }
    
    return fileFound;
}


// Tries to play a random track within the current folder.
bool VMusic3::_randomTrack(VMusicDirection direction)
{

    VMusicMode	oldMode     = mode;
    bool		found       = false;
    int			t           = 0;
    int			randomTrack = 0;

    if (diskPresent)
    {
        // You must stop a file if it's playing before you can play another one.
        if (fileIsOpen) stop();

        // We have to do an initial call to find out how many tracks are in the current directory.
		_findItemInCurrentDir(filename, VMUSIC_EMPTY_STRING, VMUSIC_ITEMTYPE_TRACK, VMUSIC_FIND_FIRST);
        if (tracks == 0) found = _findFolderWithTracks(VMUSIC_DIRECTION_NEXT, VMUSIC_FIND_FIRST);
        if (tracks > 0)
        {
            // Move through the shuffled tracks.
            // _shuffledIndex is an index that starts at 0
			_shuffledIndex += (direction == VMUSIC_DIRECTION_PREVIOUS ? -1 : 1);
            if (_shuffledIndex < 0) _shuffledIndex = tracks - 1;
            if (_shuffledIndex >= tracks) _shuffledIndex = 0;

            // Track Numbers start at 1
            randomTrack = _shuffledTracks[_shuffledIndex];

            #ifdef VMUSIC_USE_DIR_TABLE

                // Step through each item in the directory lookup table.
                for (int i = 0; i < _dirTableLength; i++)
                {
                    // Is this item a track?
                    if (!(_dirTable[i].isFolder))
                    {
                        // Have we landed on the nth track?
                        if (++t == randomTrack)
                        {
                            // Select it!
                            strlcpy(
                                filename, 
								_dirTable[i].filename,
                                sizeof(filename));
                            found = true;
                            trackNumber = randomTrack;
                            break;
                        }
                    }
                }

            #else

                // Request the entire track list.
                // This is done because we don't have a list of filenames.
                // We have to just step through to the Nth track.
				_lastCommand = VMUSIC_CMD_DIR;
				_write(VMUSIC_CMD_DIR);
				_write(VMUSIC_CHAR_RETURN);
				_waitForResponse(); // blank line

                while(1)
                {
                    // Done?
					_waitForResponse();
                    if ((_lastResponse[0] == VMUSIC_CHAR_PROMPT) && (_lastResponse[1] == VMUSIC_CHAR_NULL)) break;

                    // Found?
                    if (_isTrack(_lastResponse))
                    {
                        t++; // Track numbers start at 1.
                        
                        if (t == randomTrack)
                        {
                            strlcpy(filename, _lastResponse, sizeof(filename));
                            found = true;
                            trackNumber = randomTrack;
                        }

                        // We can't break here, because we need to read the rest of the responses to clear them.
                    }
                }

            #endif

            // File found?
            if (found) _getTrackInfo(oldMode);
        }
    }

    return found;
}


// Tries to go to the previous or next folder in the directory tree.
// Use direction = DIRECTION_PREV or DIRECTION_NEXT
//
// Returns true if successful.
bool VMusic3::_findFolder(VMusicDirection direction)
{

    char cameOutOf   [13]; // Which folder were we just in?
    char result      [13]; // Used below to find the first, prev, next, or last folder.

    bool found = false;
    bool goDeep; // ...oh my.
    
    cameOutOf[0] = VMUSIC_CHAR_NULL;
    result   [0] = VMUSIC_CHAR_NULL;
    
    // Reset the track info
    filename[0] = VMUSIC_CHAR_NULL;
    trackNumber = 0;
	_clearTrackInfo();

    if (diskPresent)
    {
        // You must stop a file if it's playing before you can play another one.
        if (fileIsOpen) stop();

        // Which direction are we going?
        if (direction == VMUSIC_DIRECTION_PREVIOUS)
        {
            goDeep = false;

            // Can we go back one level?
            if (strlen(path) == 0)
            {
                goDeep = true;
            }
            else
            {
            
                // Which folder are we coming out of?
				_getEndMostFolderFromPath(cameOutOf);                

                // Come out.
                _changeDir(VMUSIC_PARENT_DIR);

                // Look at the subfolders, and find the prev one (if any).
				_findItemInCurrentDir(result, cameOutOf, VMUSIC_ITEMTYPE_FOLDER, VMUSIC_FIND_PREVIOUS);

                // If there is one, go into it.
                if (strlen(result) > 0)
                {
                    _changeDir(result);
                    goDeep = true;
                }           
            }
            
            if (goDeep)
            {
                // Now go as deep as we can always choosing the last folder within each.
                found = false;
                while (!found)
                {
                    // Look at the subfolders, and find the last one (if any).
					_findItemInCurrentDir(result, VMUSIC_EMPTY_STRING, VMUSIC_ITEMTYPE_FOLDER, VMUSIC_FIND_LAST);

                    // Go deeper?
                    if (strlen(result) > 0) _changeDir(result);
                    else found = true;
                }
            }
        
        }
        else
        { // dir == DIRECTION_NEXT

            // Look at the subfolders, and find the first one (if any).
            #ifdef VMUSIC_USE_DIR_TABLE
                _dirTableHasBeenRead = false;
            #endif
			_findItemInCurrentDir(result, VMUSIC_EMPTY_STRING, VMUSIC_ITEMTYPE_FOLDER, VMUSIC_FIND_FIRST);

            // Can we go deeper into the tree?
            if (strlen(result) > 0)
            {
                _changeDir(result);
                found = true;
            }
            else
            {
                // Can't go deeper? Then we have to go back one level, then to the next one (if any).
                // So, are we able to go back?
                found = false;
                while ((strlen(path) > 0) && !found)
                {
                    // Which folder are we coming out of?
                    _getEndMostFolderFromPath(cameOutOf);

                    // Come out.
                    _changeDir(VMUSIC_PARENT_DIR);

                    // Look at the subfolders, and find the next one (if any).
					_findItemInCurrentDir(result, cameOutOf, VMUSIC_ITEMTYPE_FOLDER, VMUSIC_FIND_NEXT);

                    // If there is one, go into it.
                    if (strlen(result) > 0)
                    {
                        _changeDir(result);
                        found = true;
                    }
                }
            }
        }
    }

    return found;
}


// Walks the directory tree, looking for a folder with tracks in it.
// Generates the _shuffledTracks list for use in .randomMode.
//
// direction can be either previous or next
//
// trackPosition can be either first or last
//
// Returns true if successful.
bool VMusic3::_findFolderWithTracks(VMusicDirection direction, VMusicFind trackPosition)
{
    #ifdef VMUSIC_DEBUG_VERBOSE
        Serial.print("VMusic3::_findFolderWithTracks(");
        Serial.print(direction);
        Serial.print(", ");
        Serial.print(trackPosition);
        Serial.println(")");
    #endif
    
    char		pathStartedIn[VMUSIC_PATH_LENGTH + 1];
    VMusicMode  oldMode   = mode;
    bool		fileFound = false;

    filename[0] = VMUSIC_CHAR_NULL;
    trackNumber = 0;
	_clearTrackInfo();
    strlcpy(pathStartedIn, path, VMUSIC_PATH_LENGTH + 1);

    // Keep going to the prev/next folder until we find some track, or we come back to where we started.
    while (1)
    {
        // go to the Next / Prev folder.
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.println("VMusic3::go to the Next / Prev folder.");
        #endif
		_findFolder(direction);
        #ifdef VMUSIC_DEBUG_VERBOSE
            Serial.println("Done.");
        #endif
        
        // Try to find a track within the folder.
        if (randomMode)
        {
            // Random?
			_randomTrack(VMUSIC_DIRECTION_NEXT);
            fileFound = (strlen(filename) > 0);
        }
        else
        {
            // Either the FIRST or LAST track.
			_findItemInCurrentDir(filename, VMUSIC_EMPTY_STRING, VMUSIC_ITEMTYPE_TRACK, trackPosition);
            fileFound = (strlen(filename) > 0);
            if (fileFound)
            {
				_getTrackInfo(oldMode);
                trackNumber = (trackPosition == VMUSIC_FIND_FIRST) ? 1 : tracks;
            }
        }

        if (fileFound) break;

        // Just incase we've made a complete loop of all the folders, and still not found any track,
        // we better exit, otherwise it will be an infinite loop.
        if (strcmp(pathStartedIn, path) == 0) break;
    }

    // Do we need to generate the _shuffledTracks[] list?
    if (fileFound) _generateShuffledTrackList();

    return fileFound;
}


// Creates a shuffled list to traverse when in .randomMode.
// Note: "tracks" has already been counted in a previous call to _findItemInCurrentDir().
void VMusic3::_generateShuffledTrackList()
{
	unsigned int t, r;
    byte temp;

    // Reset the index.
	_shuffledIndex = 0;

    // First, create an ordered list of tracks.
    for (t = 0; t < tracks; t++) _shuffledTracks[t] = t + 1; // Track numbers start at 1.

    // Next, we shuffle them (by going through each track position, and swapping it with a random position).
    randomSeed(millis());
    for (t = 0; t < tracks; t++)
    {
        // Pick a random position to swap with.
        r = random(tracks);

        // Swap 'em.
        temp = _shuffledTracks[t];
		_shuffledTracks[t] = _shuffledTracks[r];
		_shuffledTracks[r] = temp;
    }
}


// We have to do this often enough. It might as well be a function.
void VMusic3::_clearTrackInfo()
{
    trackSeconds    = 0;
    
    // ID3 stuff.
    //track     [0] = VMUSIC_CHAR_NULL;
    title       [0] = VMUSIC_CHAR_NULL;
    artist      [0] = VMUSIC_CHAR_NULL;
    album       [0] = VMUSIC_CHAR_NULL;
    //composer  [0] = VMUSIC_CHAR_NULL;
}


// There's no command in the VMusic3 to read the ID3 info from a track.
// Instead, we need to play it.
//
// Once we've got the ID3 info, we can set it to PLAYING, PAUSED, or STOPPED.
void VMusic3::_getTrackInfo(VMusicMode newMode)
{
    byte oldVolume = volume;
    
    // Turn the volume all the way down if we're not playing.
    if (newMode != VMUSIC_MODE_PLAYING) setVolume(0);

    play(); // to get the ID3 info.

    // Turn it back up, now that we have the track info.
    if (newMode != VMUSIC_MODE_PLAYING)
    {
        stop();
        setVolume(oldVolume);
    }
}


// Waits for a response.
// Fills the _lastResponse string.
void VMusic3::_waitForResponse()
{    
    while (_getResponse() == VMUSIC_SPI_DATA_IS_OLD);
}


// Keeps checking for responses until one of the following is read:
// - Successful Command Prompt (means a disk is present)
// - No Disk
void VMusic3::_waitForPrompt()
{
    while (1)
    {
		_waitForResponse();

        // Successful Prompt
        if (strcmp(_lastResponse, VMUSIC_RESP_PROMPT) == 0) return;
        if (strcmp(_lastResponse, VMUSIC_RESP_PROMPT_EXTENDED) == 0) return;        

        // No Disk
        if (strcmp(_lastResponse, VMUSIC_RESP_ND) == 0) return;
        if (strcmp(_lastResponse, VMUSIC_RESP_ND_EXTENDED) == 0) return;
    }
}


// Fills the _lastResponse string if the data is new.
//
// Returns either SPI_DATA_IS_OLD or SPI_DATA_IS_NEW.
unsigned char VMusic3::_getResponse()
{    
    char ch;
    int  index            = 0;
    bool readingTrackTime = false;

    // Only read a response if it's new.
    while ((_read(&ch) == VMUSIC_SPI_DATA_IS_NEW))
    {
        // "T " is a special case. If we're playing, we read 'T', ' ', then two bytes, then a carriage return.
        // We need to do it this way, because otherwise 13 seconds would be interpretted as a carriage return.
        if ((mode == VMUSIC_MODE_PLAYING) && (index == 2) && (_lastResponse[0] == 'T') && (_lastResponse[1] == VMUSIC_CHAR_SPACE)) readingTrackTime = true;
        
        // Stop if a carriage return is reached (after two more byte, if reading track seconds).
        if (readingTrackTime)
        {

            if ((index >= 4) && (ch == VMUSIC_CHAR_RETURN))
            {
                #ifdef VMUSIC_DEBUG_GETRESPONSE
                    Serial.println(_lastResponse);
                #endif
                return VMUSIC_SPI_DATA_IS_NEW;
            }
        }
        else
        {
            if (ch == VMUSIC_CHAR_RETURN)
            {
                #ifdef VMUSIC_DEBUG_GETRESPONSE
                    Serial.println(_lastResponse);
                #endif
                return VMUSIC_SPI_DATA_IS_NEW;
            }
        }

        // Build the _lastResponse string.
		_lastResponse[index++] = ch;
		_lastResponse[index] = VMUSIC_CHAR_NULL;
    }

    return VMUSIC_SPI_DATA_IS_OLD;
}


// SPI isn't a strict protocol. The VMusic3 uses it's own variation.
//
// We can't use the built-in Arduino SPI.h library because the VMusic3 rely's on 
// a few extra bits (like the Address bit, and the status bit).
//
// See DS_VMUSIC3.pdf - "7.1 SPI Interface Timing" - page 7    
//
//     - Bring SS pin high 
//     - Write 1 Start bit
//     - Write 1 R/W bit                               LOW = Write                 HIGH = Read
//     - Write 1 Address bit                           LOW = Status Register       HIGH = Data Register
//     - Read or Write 8 data bits
//     - Read 1 status bit             When reading:   LOW = This data is new      HIGH = This data is old
//                                     When writing:   LOW = Write was successful  HIGH = VMusic's buffer is full :(
//     - Bring SS pin low
//     - Idle 1 cycle
unsigned char VMusic3::_transfer(unsigned char RWbit, char *pSpiData)
{
    char retData;
    unsigned char bitData;
    
    // Bring SS high to select the VMusic3
    digitalWrite(_ss, HIGH);

    // Start bit
    digitalWrite(_mosi, HIGH);
	VMUSIC_SPI_DELAY;
    digitalWrite(_sck, HIGH);
	VMUSIC_SPI_DELAY;
    digitalWrite(_sck, LOW);

    // R/W bit (LOW = Write. HIGH = Read)
    digitalWrite(_mosi, RWbit);
	VMUSIC_SPI_DELAY;
    digitalWrite(_sck, HIGH);
	VMUSIC_SPI_DELAY;
    digitalWrite(_sck, LOW);

    // ADD bit (LOW = Status Register. HIGH = Data Register)
    digitalWrite(_mosi, LOW);

	VMUSIC_SPI_DELAY;
    digitalWrite(_sck, HIGH);
	VMUSIC_SPI_DELAY;
    digitalWrite(_sck, LOW);

    // Next 8 bits - Data Phase
    bitData = 0x80;
    if (RWbit)
    {    
        // read operation (MSB transmitted first)
        retData = 0;

        while (bitData)
        {
			VMUSIC_SPI_DELAY;
            retData |= digitalRead(_miso) ? bitData : 0;
            digitalWrite(_sck, HIGH);
			VMUSIC_SPI_DELAY;
            digitalWrite(_sck, LOW);
            bitData = bitData >> 1;
        }

        *pSpiData = retData;
    }
    else
    {
        
        // write operation 
        retData = *pSpiData;

        while (bitData)
        {
            digitalWrite(_mosi, (retData & bitData) ? 1 : 0); 
			VMUSIC_SPI_DELAY;
            digitalWrite(_sck, HIGH);
			VMUSIC_SPI_DELAY;
            digitalWrite(_sck, LOW);
            bitData = bitData >> 1;
        }
    }

    // Status bit
    // For Reading:
    //     LOW     = This data is new
    //     HIGH    = This data is old. The read cycle needs to be repeated to get new data
    //
    // For Writing:
    //     LOW     = Write was successful
    //     HIGH    = VMusic3's buffer is full. Write failed
	VMUSIC_SPI_DELAY;
    bitData = digitalRead(_miso);
    digitalWrite(_sck, HIGH);
	VMUSIC_SPI_DELAY;
    digitalWrite(_sck, LOW);

    // SS goes low to disable SPI communications
    digitalWrite(_ss, LOW);

    // stay low for one clock cycle
	VMUSIC_SPI_DELAY;
    digitalWrite(_sck, HIGH);
	VMUSIC_SPI_DELAY;
    digitalWrite(_sck, LOW);

    return bitData;
}


// Reads a character from the SPI bus and returns the status bit (LOW = success. HIGH = failed).
//
// Returns either SPI_DATA_IS_OLD or SPI_DATA_IS_NEW.
unsigned char VMusic3::_read(char *pSpiData)
{
    return _transfer(VMUSIC_SPI_READ, pSpiData);
}


// Keeps trying until a character is transmitted on the SPI bus.
void VMusic3::_write(char spiData)
{    
    while (_transfer(VMUSIC_SPI_WRITE, &spiData));
}


#ifdef VMUSIC_USE_DIR_TABLE

// Reads the contents of the current directory into the _dirTable.
void VMusic3::_populateDirTable()
{
    bool isFolder       = false;
    bool isTrack        = false;

    // Start with nothing.
    tracks               = 0;
	_dirTableLength      = 0;
	_dirTableHasBeenRead = false;

    if (diskPresent)
    {
        // List files and folders in the the directory
		_lastCommand = VMUSIC_CMD_DIR;
		_write(VMUSIC_CMD_DIR);
		_write(VMUSIC_CHAR_RETURN);
		_waitForResponse();

        // Check each item returned by the DIR command.
        while(1)
        {
            // Prompt = Done.
			_waitForResponse();
            if ((_lastResponse[0] == VMUSIC_CHAR_PROMPT) && (_lastResponse[1] == VMUSIC_CHAR_NULL)) break;

            // What type of item is it?
            isFolder   = _isFolder(_lastResponse);
            isTrack    = _isTrack(_lastResponse);
            if (isFolder) _removeDirSuffix(_lastResponse);

            // Still got room in the _dirTable?
            if ((isFolder || isTrack) && (_dirTableLength < VMUSIC_DIR_TABLE_ARRAY_SIZE))
            {
                // Add it to the _dirTable?
                strlcpy(
					_dirTable[_dirTableLength].filename,
					_lastResponse,
                    13);
				_dirTable[_dirTableLength].isFolder = isFolder;

				_dirTableLength++;
            }
        }

		_dirTableHasBeenRead = true;
    }
}

#endif