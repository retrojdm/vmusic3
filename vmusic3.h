#ifndef VMUSIC3_H
#define VMUSIC3_H

#include "Arduino.h"

////////////////////////////////////////////////////////////////////////////////
// VMusic3.h
//
// By Andrew Wyatt
// retrojdm.com



////////////////////////////////////////////////////////////////////////////////
// Debugging
//
// These two options are particularly helpful in seeing what's actually going on.
// They both output to the Serial Monitor.

// Uncomment this line if you want to read what responses the VMusic is sending.
//#define VMUSIC_DEBUG_GETRESPONSE

// Uncomment this line if you want to follow the flow of the code as it happens.
//#define VMUSIC_DEBUG_VERBOSE



////////////////////////////////////////////////////////////////////////////////
// The "Directory Table" uses lots of SRAM.
//
// The Uno only has 2K of SRAM, while the MEGA has 8K.
// Using a lookup table on an Uno may cause issues, since each entry takes 14 bytes. 100 entries would be 1400 bytes (68% of available SRAM).
//
// If you comment this line out, the library won't store the directory at all, and will need to look up the directory listing each time the track progresses.
//
// If you've got the SRAM to spare, use a lookup table. Otherwise the directory listing each time you progress to another track it takes a half second or so,
// which can be annoying when you want to skip multiple tracks.
//
// Uncomment this line if you want to save SRAM
#define VMUSIC_USE_DIR_TABLE
const int VMUSIC_DIR_TABLE_ARRAY_SIZE = 100;



////////////////////////////////////////////////////////////////////////////////
//
// TODO: There's a bug when using the dir table, and going to the next folder.
//
////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////
// Constants

const byte          VMUSIC_VOLUME_MIN           =   0;
const byte          VMUSIC_VOLUME_MAX           = 254;

const unsigned int  VMUSIC_COMMAND_BUFFER       =  50;
const unsigned int  VMUSIC_RESPONSE_BUFFER      = 256;  // Don't change this one.
                                                        // The VMusic can return up to 256 chacters in a response.

                                                        // Maximum string lengths (Be careful not to run out of SRAM messing with these)
const byte          VMUSIC_ID3_STRING_LENGTH    =  30;  // ID3v1 strings are only 30 characters long.
const byte          VMUSIC_PATH_LENGTH          = 130;  // 130 characters should be enough for nesting 10 folders within eachother.

const byte          VMUSIC_MAX_SHUFFLED_TRACKS  = 100;  // 0..255. Must be the same as VMUSIC_DIR_TABLE_ARRAY_SIZE.
                                                        // A shuffled list of track numbers is generated when a new folder is opened
                                                        // (instead of just picking a random track).
                                                        // There's a limitation of doing it this way where, if there's more than this
                                                        // many tracks in a folder, it will only play these ones.

// SPI communication
const int           VMUSIC_SPI_WRITE            = LOW;
const int           VMUSIC_SPI_READ             = HIGH;

// The VMusic's SPI interface can run up to 24Mhz.
// The Arduino Uno and Mega 2560 both run at 16Mhz, so a NOP is OK to use for a a very short delay.
// However, for microcontrollers that run faster than 24Mhz we need to use delayMicroseconds().
//
// There are 1,000,000 microseconds (us) in a second.
// The VMusic has a maximum clock frequency of 24Mhz.
// This means the clock performs 24,000,000 HIGH/LOW cycles every second.
// That translates to one cycle every 0.0416us.
// In theory, a delay of 1us should be enough, but in practice I needed to use a delay of 10us for stable results.
//
//#define           VMUSIC_SPI_DELAY            __asm__("nop\n\t") // Use for 16Mhz MCUs. Eg: Arduino Uno and Mega 2560
#define             VMUSIC_SPI_DELAY            delayMicroseconds(10)

const int           VMUSIC_SPI_DATA_IS_NEW      = LOW;
const int           VMUSIC_SPI_DATA_IS_OLD      = HIGH;

// Special characters
const char          VMUSIC_CHAR_PROMPT          =  '>';
const char          VMUSIC_CHAR_RETURN          = 0x0D;
const char          VMUSIC_CHAR_SPACE           =  ' ';
const char          VMUSIC_CHAR_NULL            = '\0';

const char          VMUSIC_PARENT_DIR[]  		= "..";
const char          VMUSIC_EMPTY_STRING[]       = "";

// Command Responses
// UM_VinculumFirmware_V205.pdf - Page 18
const char          VMUSIC_RESP_PROMPT[]        =  ">"; // Successful Command Prompt
const char          VMUSIC_RESP_ND[]            = "ND"; // No Disk
const char          VMUSIC_RESP_BC[]            = "BC"; // Bad command          Command not recognised
const char          VMUSIC_RESP_CF[]            = "CF"; // Command Failed       Filename or directory name not found
const char          VMUSIC_RESP_DF[]            = "DF"; // Disk Full            No free space on disk
const char          VMUSIC_RESP_FI[]            = "FI"; // Invalid              Attempt to open a directory for reading or writing. Attempt to change currently selected directory to a file
const char          VMUSIC_RESP_RO[]            = "RO"; // Read Only            Attempt to open a read only file for writing
const char          VMUSIC_RESP_FO[]            = "FO"; // File Open            A file is currently open for writing and must be closed before this command can be executed
const char          VMUSIC_RESP_NE[]            = "NE"; // Dir Not Empty        Attempt to delete a directory which is not empty
const char          VMUSIC_RESP_FN[]            = "FN"; // Filename Invalid     Firmware invalid or contains disallowed characters
const char          VMUSIC_RESP_NU[]            = "NU"; // No Upgrade           Firmware Upgrade file not found on disk

const char          VMUSIC_RESP_PROMPT_EXTENDED[] = "D:\\>";
const char          VMUSIC_RESP_ND_EXTENDED[]   = "No Disk";
const char          VMUSIC_RESP_BC_EXTENDED[]   = "Bad Command";
const char          VMUSIC_RESP_CF_EXTENDED[]   = "Command Failed";
const char          VMUSIC_RESP_DF_EXTENDED[]   = "Disk Full";
const char          VMUSIC_RESP_FI_EXTENDED[]   = "Invalid";
const char          VMUSIC_RESP_RO_EXTENDED[]   = "Read Only";
const char          VMUSIC_RESP_FO_EXTENDED[]   = "File Open";
const char          VMUSIC_RESP_NE_EXTENDED[]   = "Dir Not Empty";
const char          VMUSIC_RESP_FN_EXTENDED[]   = "Filename Invalid";
const char          VMUSIC_RESP_NU_EXTENDED[]   = "No Upgrade";

// Monitor Events
// UM_VinculumFirmware_V205.pdf - Page 20
const char          VMUSIC_EVENT_DD1[]          = "DD1";    // Device Detected P1     Device inserted in USB Port 1
const char          VMUSIC_EVENT_DR1[]          = "DR1";    // Device Removed P1      Device removed from USB Port 1
const char          VMUSIC_EVENT_DD2[]          = "DD2";    // Device Detected P2     Device inserted in USB Port 2
const char          VMUSIC_EVENT_DR2[]          = "DR2";    // Device Removed P2      Device removed from USB Port 2
const char          VMUSIC_EVENT_SDA[]          = "SDA";    // Slave Enabled          Connected to Host Device
const char          VMUSIC_EVENT_SDD[]          = "SDD";    // Slave Disabled         Disconnected from Host Device

const char          VMUSIC_EVENT_DD1_EXTENDED[] = "Device Detected P1";
const char          VMUSIC_EVENT_DR1_EXTENDED[] = "Device Removed P1";
const char          VMUSIC_EVENT_DD2_EXTENDED[] = "Device Detected P2";
const char          VMUSIC_EVENT_DR2_EXTENDED[] = "Device Removed P2";
const char          VMUSIC_EVENT_SDA_EXTENDED[] = "Slave Enabled";
const char          VMUSIC_EVENT_SDD_EXTENDED[] = "Slave Disabled";


// Monitor Configuration Commands
// UM_VinculumFirmware_V205.pdf - Page 22
const unsigned char VMUSIC_CMD_SCS              = 0x10; // 10 0D                        Switches to shortened command set
const unsigned char VMUSIC_CMD_ECS              = 0x11; // 11 0D                        Switches to extended command set
const unsigned char VMUSIC_CMD_IPA              = 0x90; // 90 0D                        Monitor commands use ASCII values
const unsigned char VMUSIC_CMD_IPH              = 0x91; // 91 0D                        Monitor commands use binary values
const unsigned char VMUSIC_CMD_SBD              = 0x14; // 14 20 divisor 0D             Change monitor baud rate
const unsigned char VMUSIC_CMD_FWV              = 0x13; // 13 0D                        Display firmware version

// Disk Commands
// UM_VinculumFirmware_V205.pdf - Page 26
const unsigned char VMUSIC_CMD_DIR              = 0x01; // 01 0D                        List files in current directory
                                                        // 01 20 file 0D                List specified file and size
const unsigned char VMUSIC_CMD_CD               = 0x02; // 02 0D                        Change current directory
                                                        // 02 20 2E 2E 0D               Move up one directory level
const unsigned char VMUSIC_CMD_RD               = 0x04; // 04 20 file 0D                Reads a whole file
const unsigned char VMUSIC_CMD_DLD              = 0x05; // 05 20 file 0D                Delete subdirectory from current directory
const unsigned char VMUSIC_CMD_MKD              = 0x06; // 06 20 file 0D                Make a new subdirectory in the current directory
                                                        // 06 20 file 20 datetime 0D    Also specify a file date and time
const unsigned char VMUSIC_CMD_DLF              = 0x07; // 07 20 file 0D                Delete a file
const unsigned char VMUSIC_CMD_WRF              = 0x08; // 08 20 dword 0D data          Write the number of bytes specified in the 1st parameter to the currently open file
const unsigned char VMUSIC_CMD_OPW              = 0x09; // 09 20 file 0D                Open a file for writing or create a new file
                                                        // 09 20 file 20 datetime 0D    Also specify a file date and time
const unsigned char VMUSIC_CMD_CLF              = 0x0A; // 0A 20 file 0D                Close the currently open file
const unsigned char VMUSIC_CMD_RDF              = 0x0B; // 0B 20 dword 0D               Read the number of bytes specified in the 1st parameter from the currently open file
const unsigned char VMUSIC_CMD_REN              = 0x0C; // 0C 20 file 20 file 0D        Rename a file or directory
const unsigned char VMUSIC_CMD_OPR              = 0x0E; // 0E 20 file 0D                Open a file for reading
                                                        // 0E 20 file 20 date 0D        Also specify a file access date
const unsigned char VMUSIC_CMD_SEK              = 0x28; // 28 20 dword 0D               Seek to the byte position specified by the 1st parameter in the currently open file
const unsigned char VMUSIC_CMD_FS               = 0x12; // 12 0D                        Returns the free space available on disk if less than 4GB is free
const unsigned char VMUSIC_CMD_FSE              = 0x93; // 93 0D                        Returns the free space available on disk
const unsigned char VMUSIC_CMD_IDD              = 0x0F; // 0F 0D                        Display information about the disk if disk is less than 4GB
const unsigned char VMUSIC_CMD_IDDE             = 0x94; // 94 0D                        Display information about the disk
const unsigned char VMUSIC_CMD_DSN              = 0x2D; // 2D 0D                        Display disk serial number
const unsigned char VMUSIC_CMD_DVL              = 0x2E; // 2E 0D                        Display disk volume label
const unsigned char VMUSIC_CMD_DIRT             = 0x2E; // 2F 20 file 0D                List specified file and date and time of create, modify and file access

// VMUSIC Commands
// UM_VinculumFirmware_V205.pdf - Page 52
const unsigned char VMUSIC_CMD_VPF              = 0x1D; // 1D 20 file 0D                Plays a single file
const unsigned char VMUSIC_CMD_VRF              = 0x89; // 89 20 file 0D                Repeatedly plays a single file
const unsigned char VMUSIC_CMD_VST              = 0x20; // 20 0D                        Stops playback
const unsigned char VMUSIC_CMD_V3A              = 0x21; // 21 0D                        Plays all MP3 files
const unsigned char VMUSIC_CMD_VRA              = 0x8A; // 8A 0D                        Repeatedly plays all MP3 files
const unsigned char VMUSIC_CMD_VRR              = 0x8F; // 8F 0D                        Repeatedly plays random MP3 files
const unsigned char VMUSIC_CMD_VSF              = 0x25; // 25 0D                        Skip forward one track
const unsigned char VMUSIC_CMD_VSB              = 0x26; // 26 0D                        Skip back one track
const unsigned char VMUSIC_CMD_VSD              = 0x8E; // 8E 0D                        Skip forward one whole directory
const unsigned char VMUSIC_CMD_VP               = 0x8B; // 8B 0D                        Pause playback
const unsigned char VMUSIC_CMD_VF               = 0x8C; // 8C 0D                        Fast forward 5 seconds
const unsigned char VMUSIC_CMD_VB               = 0x8D; // 8D 0D                        Rewind 5 seconds
const unsigned char VMUSIC_CMD_VRD              = 0x1F; // 1F 20 byte 0D                Reads command register
const unsigned char VMUSIC_CMD_VWR              = 0x1E; // 1E 20 byte word 0D           Writes command register
const unsigned char VMUSIC_CMD_VSV              = 0x88; // 88 20 byte 0D                Sets playback volume


////////////////////////////////////////////////////////////////////////////////
// enums

enum VMusicEvent
{
    VMUSIC_EVENT_NONE,
    VMUSIC_EVENT_INITIALISED,
    VMUSIC_EVENT_USB_DETECTED,
    VMUSIC_EVENT_USB_REMOVED,
    VMUSIC_EVENT_TRACK_STOPPED,
    VMUSIC_EVENT_TRACK_CHANGED,
    VMUSIC_EVENT_TRACK_PROGRESSED
};

// .mode
enum VMusicMode
{
    VMUSIC_MODE_STOPPED,
    VMUSIC_MODE_PLAYING,
    VMUSIC_MODE_PAUSED
};

// .findTrack() and .findFolder()
enum VMusicDirection
{
    VMUSIC_DIRECTION_PREVIOUS,
    VMUSIC_DIRECTION_NEXT
};

enum VMusicItemType
{
    VMUSIC_ITEMTYPE_TRACK,
    VMUSIC_ITEMTYPE_FOLDER
};

enum VMusicFind
{
    VMUSIC_FIND_FIRST,
    VMUSIC_FIND_PREVIOUS,
    VMUSIC_FIND_NEXT,
    VMUSIC_FIND_LAST
};


////////////////////////////////////////////////////////////////////////////////
// Class Definition

class VMusic3 {
    
public:     
    VMusic3(
        int miso,    // MISO = brown wire
        int mosi,    // MOSI = orange wire
        int sck,     // SCK  = yellow wire
        int ss       // SS   = green wire
        );


    ////////////////////////////////////////////////////////////////////////////////
    // Public Attributes

    bool            initialised;    // We can't send commands until we're in "Short Command Set" mode.

    byte            volume;         // 0..254
    bool            keepPlaying;    // Try to play another track when the current one finishes?
    bool            repeatFolder;   // Play only tracks within the current folder?
    bool            randomMode;     // Play a random track within the current folder?

    // Track info
    unsigned int    tracks;         // Number of tracks in current folder (updated by findItem() )
    unsigned int    trackNumber;    // Position of track in the folder
    unsigned int    trackSeconds;   // When playing, how far into the song are we?
    //char          track           [VMUSIC_ID3_STRING_LENGTH + 1]; // To try and save SRAM, we're not going to store the track.
    char            title           [VMUSIC_ID3_STRING_LENGTH + 1];
    char            artist          [VMUSIC_ID3_STRING_LENGTH + 1];
    char            album           [VMUSIC_ID3_STRING_LENGTH + 1];
    //char          composer        [VMUSIC_ID3_STRING_LENGTH + 1]; // To try and save SRAM, we're not going to store the composer.
    
    VMusicMode		mode;           // stopped, playing, paused.
    bool            diskPresent;
    bool            fileIsOpen;

                                    // "path" can potentially be a very long string. Luckily everything's in 8.3 format!
                                    // 131 bytes allows us to go a max of 10 levels deep ((12 chars + '/' * 13) + null terminator)
                                    // If your USB is organised into more than 10 levels of sub-directories, you're gonna have problems.
    char            path            [VMUSIC_PATH_LENGTH + 1];

    char            filename        [13];   // 8.3 format = 12 characters + null terminator.


    ////////////////////////////////////////////////////////////////////////////////
    // Public Functions

    VMusicEvent		loop            ();					// Returns an event code if something changed.
    void            setVolume       (byte newVolume);   // min 0..254 max
	bool            loadTrack		(const char *newPath, const char *newFilename, int newTrackNumber, VMusicMode newMode);
    
    // Player controls.
    void            play            ();
    void            pause           (); // Toggles between playing and paused.
    void            stop            ();
    void            fastForward     ();
    void            rewind          ();
    bool            prevTrack       ();
    bool            nextTrack       ();
    bool            prevFolder      ();
    bool            nextFolder      ();



private:

    ////////////////////////////////////////////////////////////////////////////////
    // Private Attributes

    int             _miso, _mosi, _sck, _ss;	

	char            _lastCommand;
    char            _lastResponse           [VMUSIC_RESPONSE_BUFFER + 1];
    bool            _justSeeked;        // This is a hack to try and fix the bug where a prompt is sometimes muddled with the "T xx" response
                                        // after a fastForward() or rewind() command while playing.
    
    bool            _lookingForTrack;   // This is set by vmusic.loop() when it sees a DD2 event.
                                        // In your sketch's loop, watching for .diskPresent changing to true allows you to
                                        // display a "Searching..." string and perhaps try to load a specific track with
                                        // the loadTrack() function.
                                        // Otherwise, the next pass of the loop will try to find the first track.

    byte            _shuffledTracks         [VMUSIC_MAX_SHUFFLED_TRACKS]; // The list of tracks, shuffled when the folder a new opened.
    unsigned int    _shuffledIndex;


    #ifdef VMUSIC_USE_DIR_TABLE
        
    // Each entry in the dirTable has a filename, and flag indicating if it's a folder or not.
    typedef struct {
        char filename[13];
        bool isFolder;
    } DirTableItem;
    
    DirTableItem _dirTable                  [VMUSIC_DIR_TABLE_ARRAY_SIZE]; // The lookup table.
    
    int _dirTableLength;                   // How many tracks in the current dir?

    bool _dirTableHasBeenRead;              // This is reset when the directory is changed.
                                            // The first call to getItem() will need to read the directory.
    #endif


    ////////////////////////////////////////////////////////////////////////////////
    // Private Functions

	VMusicEvent 	_handleResponse					();
	VMusicEvent		_handleResponsePrompt			();
	VMusicEvent		_handleResponseTrackSeconds		();
	VMusicEvent		_handleResponseStopped			();
	VMusicEvent		_handleResponseDeviceDetected	();
    VMusicEvent     _handleResponseNoUpgrade        ();
	VMusicEvent		_handleResponseDeviceRemoved	();
	VMusicEvent		_handleResponseBlank			();
	VMusicEvent		_handleResponseVersion			();
    VMusicEvent     _handleResponseNoDisk           ();
	VMusicEvent		_handleResponsePromptExtended	();
    VMusicEvent     _handleResponseCommandFailed    ();    

    void            _tryToEnterScsMode              ();
    void            _markAsStopped                  ();

    bool            _isFolder					(const char *filenameToCheck);
    bool            _isTrack					(const char *filenameToCheck);
    void            _removeDirSuffix            (char *folderResponse);
    bool            _trackExists				(const char *filenameToFind);
    bool            _changeDir					(const char *newDir);
    
    void            _getEndMostFolderFromPath	(char *result);
	void            _findItemInCurrentDir		(char *result, const char *currentItem, VMusicItemType findItemType, VMusicFind position);
    bool            _findTrack					(VMusicDirection direction);
    bool            _randomTrack				(VMusicDirection direction);
    bool            _findFolder					(VMusicDirection direction);
    bool            _findFolderWithTracks		(VMusicDirection direction, VMusicFind trackPosition);

    void            _generateShuffledTrackList	();
    void            _clearTrackInfo				();
    void            _getTrackInfo				(VMusicMode newMode);
    
    void            _waitForResponse			();
    void            _waitForPrompt				();
    unsigned char   _getResponse				();
    unsigned char   _transfer					(unsigned char RWbit, char *pSpiData);
    unsigned char   _read						(char *pSpiData);
    void            _write						(char spiData);

    #ifdef VMUSIC_USE_DIR_TABLE

    void            _populateDirTable			();

    #endif
};

#endif