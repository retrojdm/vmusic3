VMusic3 Arduino Library
=======================

By Andrew Wyatt

retrojdm.com


--------------------------------------------------------------------------------


Powering via a digital IO pin
-----------------------------
If you power the VMusic3 directly with 5V, when you reset the Arduino the
VMusic3 won't reset properly.

You _could_ always disconnect then reconnect the Arduino's power, but that can
be inconvenient.

I wanted the VMusic3 to reset each time I reset the Arduino or clicked "Upload".
So, to ensure the VMusic3 is reset when the Arduino is, I use a digital IO to
power it.

There's a problem though...
The digital IO's don't provide enough current to power the VMusic3 (USB sticks
suck down a lot of power!).

As a solution, I connect the VMusic3's 5V power via an NPN transistor. You could also use a relay.
In sketches, before calling vmusic.begin(), I first write HIGH to the digital
IO.

			   _________
	         /           \
	        /    BC548    \
	       / NPN Transistor\
	       |_______________|
	         |     |     |
	         |     |     |
	         |     |     |
	(collector) (base) (emitter)
	   Arduino   IO     VMusic3
	        5V   pin    5V



SPI
---
This library uses SPI to communicate with the VMusic3.
You'll need to change the UART/SPI jumper to SPI mode (middle pin to GND pin).



VMusic2 vs VMusic3
------------------
I only have a VMusic3 to test with, so I'm not sure if the VMusic2 works with
this library.
The firmware this library was developed on is "V2MSC1.0.0"
If your VMusic2 doesn't work, you could try upgrading to that firmware.



5V or 3.3V data?
----------------
I've checked the IO lines with an oscilliscope, and they're 3.3v. But they still work on 5V systems.
I've tested successfully with 5v boards: (Arduino Uno R3 and Mega 2560) and 3.3v boards (PJRC Teensy 3.6).


Running out of SRAM
-------------------
The VMusic3 class can potentially use a lot of SRAM. The 5x 256 byte ID3 fields
add up to 1285 bytes (including the null terminators).
The Uno only has 2k available :(

To conserve SRAM, I'm ignoring the Track and Composer ID3 fields, and only
storing the first 30 characters of the Title, Artist, and Album fields.
To increase/decrease the 30 character limit, change VMUSIC_ID3_STRING_LENGTH
below.

Also, I'm limiting the path length to 130 characters. This is enough for
nesting at least 10 folders within eachother (since each folder name can be up
to 12 characters long, plus the backslashes).
Again, you can change VMUSIC_PATH_LENGTH below, depending on your needs.

Another way to save SRAM is to disable the Lookup Table (which saves the contents of the directory when first listed) by commenting out the line:
```
#!arduino
#define USE_DIR_TABLE
```

See http://arduino.cc/en/Tutorial/Memory
"If you run out of SRAM, your program may fail in unexpected ways; it will
appear to upload successfully, but not run, or run strangely."